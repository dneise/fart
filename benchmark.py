#!/usr/bin/python -tt

import sys
import fart.io_utils.FitsReader as FitsReader
import fart.drs_spikes as drs_spikes

    
f = FitsReader.EventGetter( sys.argv[1], sys.argv[2] )
despiker = drs_spikes.DRSSpikes()

for index,event in enumerate(f):
    data = despiker(event)
    if index > 10:
        break
