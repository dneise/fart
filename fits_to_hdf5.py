#!/usr/bin/python -tt

import sys
import os.path as path
import h5py

import fart.io_utils.FitsReader_new as FitsReader
import fart.drs_spikes as drs_spikes


data_filepath = sys.argv[1]
calib_filepath = sys.argv[2]
out_filepath = sys.argv[3]
data_filename = path.split( data_filepath )[1]
print "data filename:", data_filename

f = FitsReader.EventGetter( data_filepath, calib_filepath )
outfile = h5py.File(out_filepath, "w")

number_of_events = f.data_getter.header_values['NAXIS2']
number_of_pixel = f.data_getter.header_values['NBOARD'] * 36
roi = f.data_getter.header_values['NROI']
print "number of events - from header:", number_of_events
print "number_of_pixel:", number_of_pixel
print "roi:", roi

# the axes in the dest are chosen, such slowest changing one is leftmost
# left: event_number
# middle: pixel_number
# right: time  
dset = outfile.create_dataset(data_filename, 
                        shape=(number_of_events, number_of_pixel, roi), 
                        dtype='f4', 
                        chunks=(1,number_of_pixel, roi) )

for key in f.data_getter.header_values:
    dset.attrs[key] = f.data_getter.header_values[key]

# We are especially interested in the duration of a run, 
# and since the start and stoptime of a run, is stored in a funny way in the 
# FITS files, we have to treat them a bit, before we get the actual numbers
# shortcuts:
header = f.data_getter.header_values
# time_stamps are in seconds since 01.01.1970  - unix epoch
stop_time_stamp = (header['TSTOPI'] + header['TSTOPF']) * 24 * 60 * 60 
start_time_stamp = (header['TSTARTI'] + header['TSTARTF']) * 24 * 60 * 60 
duration = stop_time_stamp - start_time_stamp
dset.attrs["duration"] = duration 

for name, value in dset.attrs.iteritems():
  print name+":", value

despiker = drs_spikes.DRSSpikes()

for index,event in enumerate(f):
    print "event:", index
    data = despiker(event)
    dset[index] = data[:number_of_pixel]

dset.resize( (index+1, number_of_pixel, roi) )

outfile.close()
