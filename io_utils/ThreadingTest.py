import readline
import rlcompleter
import re
readline.parse_and_bind('tab: complete')
readline.parse_and_bind('Control-Space: complete')
readline.set_completer_delims(re.compile(r'[\'"\\[]').sub('', readline.get_completer_delims())) 

import gzip
import numpy as np
import numexpr as ne

import threading
import Queue
import sys




class Reader(threading.Thread):
    FileLock = threading.Lock()
    
    def __init__(self, filename):
        super(A, self).__init__()
        
    def run(self):
        while True:
            Reader.FileLock.acquire()
            Q.put(f.read(4*1024*1024), block=True)
            Reader.FileLock.release()
            
class Calibrator( threading.Thread ):
    tasks = Queue.Queue( maxsize = 10)
    
    events = {}
    eventLock = threading.Lock()
    
    def run( self ):
        while True:
            
            event = Calibrator.tasks.get()
            
            cleaned_event = self.calibrate(event)
            
            Calibrator.eventLock.acquire()
            Calibrator.events[cleaned_event.id]=cleaned_event
            Calibrator.eventLock.release()
            
            Calibrator.tasks.task_done()
            
    def calibrate(self, event):
        # shortcuts
        
        blm = event.blm
        gm  = event.gm
        tom = event.tom

        to_mV = 2000./4096.
        data = event.rawdata
        start_cells = event.sc
        data = data.astype('float32')
        data *= to_mV
        roi = event.roi
        
        for p in range(1440):
            pdata = data[p]
            sc = start_cells[p]
            
            b1 = blm[p,sc:sc+roi]
            t1 = tom[p,:roi]
            g1 = gm[p,sc:sc+roi]
            #pdata = ne.evaluate("(pdata - b1 - t1) / g1")
            pdata = (pdata - b1 - t1) / g1
            data[p] = pdata
        data *= 1907.35
        
        event.data = data
        return event




class BasicEventGetter ( object ):
    def __init__( self, filename ):
        
        if ".gz" in filename:
            self._f = gzip.GzipFile(filename)
        else:
            self._f = open(filename)
        
        # our FITS files have two headers, lets read the first chunk.
        # headers consist of multiples of 2880 bytes
        h_chunk = self._f.read(2880)
        # we need to split up the header, into "lines" of 80 bytes
        # this makes 36 lines of 80 bytes in a chunk of 2880
        h1 = [ h_chunk[s*80:(s+1)*80] for s in range(36) ]
        # in order to find out, if this chunk, was a complete 
        # header, one of the lines must contain the letters "END" 
        # in the first 3 bytes
        end_found = False
        for line in h1:
            if line[0:3] == "END":
                end_found = True
                break
        if not end_found:
            raise IOError("first header was longer than 2880 bytes. Strange!")
        self._h1 = h1
        
        # Now we need to read the second header, this header
        # might change in size
        # so let's read the first chunk
        h_chunk = self._f.read(2880)
        h2 = [ h_chunk[s*80:(s+1)*80] for s in range(36) ]
        end_found = False
        for line in h2:
            if line[0:3] == "END":
                end_found = True
                break
        while not end_found:
            h_chunk = self._f.read(2880)
            h2 += [ h_chunk[s*80:(s+1)*80] for s in range(36) ]
            for line in h2:
                if line[0:3] == "END":
                    end_found = True
                    break
        self._h2 = h2

        # This member needs to be set by somebody
        self.rawdata = None
    
    def __iter__( self ):
        return self
        
    def next( self ):
        # read unzipped data
        s = self._f.read( self.rawdata.dtype.itemsize )
        try:
            event = np.fromstring(s, dtype=self.rawdata.dtype, count=1)
        except ValueError:
            raise StopIteration
        return event

        
class RawEventGetter( BasicEventGetter ):
    
    def __init__( self, filename ):
        super(RawEventGetter, self).__init__(filename)
        
        # now we need one single info from the seconds header.
        # This is the value NROI
        for line in self._h2:
            if "NROI" in line:
                line = line.split()
                if line[0] == "NROI":
                    self.roi = int(line[2])
            if "NAXIS2" in line:
                line = line.split()
                self.nevents = int(line[2])
        
        # And now we define an numpy.recarray
        # which will be used to hold one single event
        # The format of this array, can be determined
        # from the seconds header, but I hardcode it here, because I'm lazy.
        self.rawdata = np.recarray(1, dtype = np.dtype([
            ('EventNum', '>u4'), 
            ('TriggerNum', '>u4'), 
            ('TriggerType', '>u2'), 
            ('NumBoards', '>u4'), 
            ('Errors', '|u1', 4), 
            ('SoftTrig', '>u4'), 
            ('UnixTimeUTC', '>u4', 2), 
            ('BoardTime', '>u4', 40), 
            ('StartCellData', '>u2', 1440), 
            ('StartCellTimeMarker', '>u2', 160), 
            ('Data', '>i2', (1440,self.roi) ) 
            ] ) )


class CalibrationConstantGetter( BasicEventGetter ):
    
    def __init__( self, filename ):
        super(CalibrationConstantGetter, self).__init__(filename)
        
        # now we need one single info from the seconds header.
        # This is the value NROI
        for line in self._h2:
            if "NROI" in line:
                line = line.split()
                self.roi = int(line[2])
                break
        
        self.rawdata = np.recarray(1, dtype = np.dtype([
            ('RunNumberBaseline', '>u4'), 
            ('RunNumberGain', '>u4'), 
            ('RunNumberTriggerOffset', '>u4'),
            ('BaselineMean', '>f4', (1440,1024)),
            ('BaselineRms', '>f4', (1440,1024)),
            ('GainMean', '>f4', (1440,1024)),
            ('GainRms', '>f4', (1440,1024)),
            ('TriggerOffsetMean', '>f4', (1440,self.roi)),
            ('TriggerOffsetRms', '>f4', (1440,self.roi)) ]) )
            

class Event( object ):
    rawdata = None
    blm = None
    tom = None
    gm = None
    sc = None
    roi = None
    data =None
    id = None

class EventGetter( object ):
    def __init__(self, data_file_name, calib_file_name ):
        self.data_getter = RawEventGetter( data_file_name )
        calib_file = CalibrationConstantGetter( calib_file_name )
        self.calib_file =calib_file
        calib = calib_file.next()
        self.calib = calib
        self.roi = self.data_getter.roi
        self.nevents = self.data_getter.nevents
        
        ###########
        blm = self.calib['BaselineMean'][0]
        gm  = self.calib['GainMean'][0]
        tom = self.calib['TriggerOffsetMean'][0]
        blm = np.concatenate( (blm,blm), axis=1 )
        gm = np.concatenate( (gm,gm), axis=1 )

        self.blm = blm
        self.gm = gm
        self.tom = tom

    def __iter__(self):
        """ iterator """
        return self

    def next(self):
        bla = self.data_getter.next()
        event = Event()
        event.rawdata = bla['Data'][0]
        event.blm = self.blm
        event.tom = self.tom
        event.gm = self.gm
        event.sc = bla['StartCellData'][0]
        event.roi = self.roi
        event.id = 1
        
        return event
        
filename = sys.argv[1]
calibname = sys.argv[2]
eg = EventGetter( filename,calibname )

my_threads = [ Calibrator() for i in range(4) ]
for thread in my_threads:
    thread.setDaemon(True)
    thread.start()


for i,event in enumerate(eg):
    Calibrator.tasks.put( event , block=True)
    if i > 400:
        break
