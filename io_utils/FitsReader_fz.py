import sys
import readline
import rlcompleter
readline.parse_and_bind('tab: complete')

import re
import gzip
import numpy as np

def split_into_lines(chunk):
    # we need to split up the header, into "lines" of 80 bytes
    # this makes 36 lines of 80 bytes in a chunk of 2880
    return [ chunk[s*80:(s+1)*80] for s in range(36) ]

def contains_end( header):
    """ header is a list of 36 strings (each 80 bytes long) """
        # in order to find out, if this chunk, was a complete 
        # header, one of the lines must contain the letters "END" 
        # in the first 3 bytes
    for line in header:
        if line[0:3] == "END":
            return True
    return False

def take_apart( header ):
        # The FTIS header is just a set of 80 character lines....
        # one line looks like
        # CREATOR = 'datalogger'         / Program that wrote this file
        # REVISION= '15107   '           / SVN revision
        # TSTARTI =                15803 / Time when first event received
        #
        # 1st an uppercase key-word (max 8 chars)
        # 2nd and "=" sign (can be used for cutting)
        # 3rd the value:
        #       * mostly string in single qoutes 'string'
        #       * sometimes an int or float
        #       * sometimes just a single char "T" or "F"
        # 4th the "/" comment delimiter
        # 5th the comment just a string of chars
        # 6th spaces to fill up the 80chars of each line
        
        # Note: In case the value is a quoted 'string' it can be so long,
        # that no place for the comment is left ... so the comment delimiter
        # "/" can be missing
        
        # We will use this information to smartly parse the header info 
        # into a dict like:
        header_values = {}
        header_smart_values = {}
        header_comments = {}

        header_dict = {}
        # header_dict[key] = (value, comment)

        # TODO: check if this can be done cleaner and shorter with regular expressions
        # example code snippet:
        #
        #   import re
        #   logstring = '[1242248375] SERVICE ALERT: myhostname.com;DNS: Recursive;CRITICAL;SOFT;1;CRITICAL - Plugin timed out while executing system call'
        #   exp = re.compile('^\[(\d+)\] ([A-Z ]+): ([A-Za-z0-9.\-]+);[^;]+;([A-Z]+);')
        #   m = exp.match(logstring)
        #   for s in m.groups():
        #        print s


        for line in header:
            line = line.strip()
            if len(line) ==0:
                continue
            if "END" == line:
                break
            try:
                key_value, comment = line.split('/',1)
            except ValueError:
                comment = ''
                key_value = line
            try:
                key, value = key_value.split('=',1)
            except ValueError:
                # This line is not a valid header line ... 
                continue
            key = key.strip()
            value = value.strip()
            header_dict[key] = (value, comment)
        return header_dict

def guess_type( header_dict ):
    smart_header = {}
    for key in header_dict:
            #TODO: reduce identation with better editor.

            # value is a string ... we need to check, what type it might be...
            value, comment = header_dict[key]

            smart_value = None
            
            # lets first check for 'T' and 'F'
            if value == "T":
                smart_value = True
            elif value == "F": 
                smart_value = False
            else:
                # it might still be a one char integer .. like '1'
                pass

            # so lets now check for integers, but only as long as 
            # smart_value is not already assigned
            if smart_value is None:
                try:
                    smart_value = int(value)
                except ValueError:
                    # it was not an integer
                    pass

            # so let's now try to cast it into a float
            if smart_value is None:
                try:
                    smart_value = float(value)
                except ValueError:
                    # it was not an integer
                    pass

            # If we still have not been able to assign a smart_value, it 
            # must be a string ... but this means it must be enclosed 
            # in single quotes
            if smart_value is None:
                if len(value) >= 2: # must be if enclosed in single quotes
                    if value[0]=="'" and value[-1]=="'":
                        smart_value = value[1:-1]

            # Well .. if we still not were able smart_parse the value
            # there is something wrong our assumptions.
            # But what should we do?!

            smart_header[key] = (smart_value, comment)
    return smart_header


class BasicEventGetter ( object ):
    def __init__( self, filename ):
        if ".gz" in filename:
            self._f = gzip.GzipFile(filename)
        else:
            self._f = open(filename)
        
        # our FITS files have two headers, lets read the first chunk.
        # headers consist of multiples of 2880 bytes
        h1 = split_into_lines( self._f.read(2880) )
        if not contains_end(h1):
            raise IOError("first header was longer than 2880 bytes. Strange!")
        self._h1 = h1
        
        # Now we need to read the second header, this header
        # might change in size
        # so let's read the first chunk
        h2 = split_into_lines( self._f.read(2880) )
        while not contains_end(h2):
            h2 += split_into_lines( self._f.read(2880) )
        self._h2 = h2

        self._smart_parse_header( self._h2 )
        self._smart_parse_TFIELDS()

        start = self._f.tell()
        s = self._f.read( self.dtype.itemsize )
        ZDrsCellOffsets_Description = np.fromstring(s, dtype=self.dtype, count=1)
        N_bytes = ZDrsCellOffsets_Description['OffsetCalibration_N'][0]
        offset = ZDrsCellOffsets_Description['OffsetCalibration_Offset'][0]
        self._f.seek(start)
        self._f.seek(self.header['THEAP'][0],1)
         
        cell_offsets_dtype = np.dtype([('offsets','|u1',N_bytes)])
        self._f.seek(offset,1)
        s = self._f.read( cell_offsets_dtype.itemsize )

        self.cell_offsets = np.fromstring(s, cell_offsets_dtype, count=1)
        self._f.seek( int(np.ceil(float(self._f.tell())/2880))*2880 )
        
        h3 = split_into_lines( self._f.read(2880) )
        while not contains_end(h3):
            h3 += split_into_lines( self._f.read(2880) )
        self._h3 = h3
        
        self._smart_parse_header( self._h3 )
        self._smart_parse_TFIELDS()
        self.start_of_2nd_heap = self._f.tell() + self.header['THEAP'][0]

    def __iter__( self ):
        return self
        
    def next( self ):
        # read unzipped data
        s = self._f.read( self.dtype.itemsize )
        try:
            event = np.fromstring(s, dtype=self.dtype, count=1)

        except ValueError:
            raise StopIteration
        return event
    
    def _smart_parse_header( self, header_lines  ):
        header = take_apart( header_lines )
        self.header = guess_type( header )


    def _smart_parse_TFIELDS( self ):
        h = self.header
        try:
            n_fields=h['TFIELDS'][0]
        except KeyError:
            print "Warning: Fits header field TFIELDS was not found"
            return
        
        # We expect now to find the fields TTYPE## and TFORM## in the header
        # ## - denotes a number from 1 to n_fields
        # examples are:
        # TTYPE1  = 'Time    '
        # TFORM1  = '1D      '
        
        # the letters denote the data type...
        # we need to translate from Fits data type letter code to numpy
        # data type:
        type_map = {}
        type_map['L'] = '?'
        type_map['B'] = '|u1'
        type_map['I'] = '>u2'
        type_map['J'] = '>u4'
        type_map['K'] = '>u8'
        type_map['A'] = '|S'
        type_map['E'] = '>f4'
        type_map['D'] = '>f8'
    
        # We do something secial here .. since we are dealing with FACT fits 
        # files mostly, we will treat those columns special, that have to do
        # with Pixels. Therefore we need to know how many pixel there are.
        try:
            n_pixel = h['NPIX'][0]
        except KeyError:
            n_pixel = None
            
        dtype_list = []
        vla_dtype = {}
        for i in range(1,n_fields+1):
            name = h['TTYPE%d'%i][0].strip()
            form = h['TFORM%d'%i][0].strip()
            # the form is mosty a  number followed by one or more capital letters
            # like e.g.: "1234J", "1E" , "1QB",  ...
            # but it can also be just some cpital letters, which means, the number is 1
            try:
                num = int(re.sub("[^0123456789]","", form))  # ^ means 'not' in regular expressions
            except ValueError:
                num = 1
            letters = re.sub("[0123456789]","", form)        # note: the ^ is missing
            if letters[0] == 'Q':
                dt1 = (name+'_N', '>u8', 1)
                dt2 = (name+'_Offset', '>u8', 1)
                dtype_list.append( dt1 )
                dtype_list.append( dt2 )
                vla_dtype[name] = type_map[letters[1]]
            elif letters[0] == 'P':
                dt1 = (name+'_N', '>u4', 1)
                dt2 = (name+'_Offset', '>u4', 1)
                dtype_list.append( dt1 )
                dtype_list.append( dt2 )
                vla_dtype[name] = type_map[letters[1]]
            elif letters == 'A':
                dtype_tuple = (name, "%s%d"%(type_map[letter],num))
                dtype_list.append( dtype_tuple )
            else:
                # Now we will check if this column might contain data
                # pixel-wise.
                if n_pixel is not None and num%n_pixel == 0:
                    num = (n_pixel, num/n_pixel)
                dtype_tuple = (name, type_map[letter], num)
                dtype_list.append( dtype_tuple )
            
        self.dtype = np.dtype(dtype_list)
        self.vla_dtype = vla_dtype
        
if __name__ == '__main__':
  from pprint import pprint
  import sys
  eg = BasicEventGetter(sys.argv[1])

