import sys
import readline
import rlcompleter
readline.parse_and_bind('tab: complete')

import gzip
import numpy as np

class BasicEventGetter ( object ):
    def __init__( self, filename ):
        if ".gz" in filename:
            self._f = gzip.GzipFile(filename)
        else:
            self._f = open(filename)
        
        # our FITS files have two headers, lets read the first chunk.
        # headers consist of multiples of 2880 bytes
        h_chunk = self._f.read(2880)
        # we need to split up the header, into "lines" of 80 bytes
        # this makes 36 lines of 80 bytes in a chunk of 2880
        h1 = [ h_chunk[s*80:(s+1)*80] for s in range(36) ]
        # in order to find out, if this chunk, was a complete 
        # header, one of the lines must contain the letters "END" 
        # in the first 3 bytes
        end_found = False
        for line in h1:
            if line[0:3] == "END":
                end_found = True
                break
        if not end_found:
            raise IOError("first header was longer than 2880 bytes. Strange!")
        self._h1 = h1
        
        # Now we need to read the second header, this header
        # might change in size
        # so let's read the first chunk
        h_chunk = self._f.read(2880)
        h2 = [ h_chunk[s*80:(s+1)*80] for s in range(36) ]
        end_found = False
        for line in h2:
            if line[0:3] == "END":
                end_found = True
                break
        while not end_found:
            h_chunk = self._f.read(2880)
            h2 += [ h_chunk[s*80:(s+1)*80] for s in range(36) ]
            for line in h2:
                if line[0:3] == "END":
                    end_found = True
                    break
        self._h2 = h2

        # This member needs to be set by somebody
        self.rawdata = None
        self._smart_parse_header()
        self._smart_parse_TFIELDS()
    
    def __iter__( self ):
        return self
        
    def next( self ):
        # read unzipped data
        s = self._f.read( self.rawdata.dtype.itemsize )
        try:
            event = np.fromstring(s, dtype=self.rawdata.dtype, count=1)
        except ValueError:
            raise StopIteration
        return event
        
    def _parse_header( self, int_keys, float_keys ):
        # Some of the values, should be parsed to ints or floats,
        # some should stay as they are, as strings.
        # Here I give two lists of keys, which should be converted
        # examples for parameters
        #int_keys = ['NROI', 'NAXIS1', 'NAXIS2', 
        #            'TSTARTI', 'TSTOPI' , 
        #            'NBOARD']
        #float_keys = ['TSTARTF', 'TSTOPF']

        # We are going to put all information from the second header
        # into two dicts, one for the values, one for the comments
        header_values = {}
        header_comments = {}

        
        for line in self._h2:
            if not '/' in line:
                continue
            content, comment = line.split('/',1)
            content = content.strip()
            comment = comment.strip()
            if not '=' in content:
                continue
            key, value = content.split('=',1)
            key = key.strip()
            value = value.strip()
            if key in int_keys:
                value = int(value)
            if key in float_keys:
                value = float(value)
            header_values[key] = value
            header_comments[key] = value
                
        self.header_values = header_values
        self.header_comments = header_comments

    def _smart_parse_header( self ):
        # The FTIS header is just a set of 80 character lines....
        # one line looks like
        # CREATOR = 'datalogger'         / Program that wrote this file
        # REVISION= '15107   '           / SVN revision
        # TSTARTI =                15803 / Time when first event received
        #
        # 1st an uppercase key-word (max 8 chars)
        # 2nd and "=" sign (can be used for cutting)
        # 3rd the value:
        #       * mostly string in single qoutes 'string'
        #       * sometimes an int or float
        #       * sometimes just a single char "T" or "F"
        # 4th the "/" comment delimiter
        # 5th the comment just a string of chars
        # 6th spaces to fill up the 80chars of each line
        
        # Note: In case the value is a quoted 'string' it can be so long,
        # that no place for the comment is left ... so the comment delimiter
        # "/" can be missing
        
        # We will use this information to smartly parse the header info 
        # into a dict like:
        header_values = {}
        header_smart_values = {}
        header_comments = {}

        for line in self._h2:
            line = line.strip()
            if len(line) ==0:
                continue
            if "END" == line:
                break

            if not '=' in line:
                print "no = in line"
                print line
                continue

            result = line.split('/',1)
            content = result[0].strip()
            try:
                comment = result[1].strip()
            except IndexError:
                comment = None
            key, value = content.split('=',1)
            key = key.strip()
            value = value.strip()
            
            # now we smartly try to check what type the value is
            smart_value = None
            # lets first check for 'T' and 'F'
            if len(value) == 1:
                if value == "T":
                    smart_value = True
                elif value == "F": 
                    smart_value = False
                else:
                    # it might still be a one char integer .. like '1'
                    pass
            # so lets now check for integers, but only as long as 
            # smart_value is not already assigned
            if smart_value is None:
                try:
                    smart_value = int(value)
                except ValueError:
                    # it was not an integer
                    pass
            # so lets' now try to cast it into a float
            if smart_value is None:
                try:
                    smart_value = float(value)
                except ValueError:
                    # it was not an integer
                    pass
            # If we still have not been able to assign a smart_value, it 
            # must be a string ... but this means it must be enclosed 
            # in single quotes
            if smart_value is None:
                if len(value) >= 2: # must be if enclosed in single quotes
                    if value[0]=="'" and value[-1]=="'":
                        smart_value = value[1:-1]
            # Well .. if we still not were able smart_parse the value
            # there is something wrong our assumptions.

            header_values[key] = value
            header_comments[key] = comment
            header_smart_values[key] = smart_value

        self.header_values = header_values
        self.header_comments = header_comments
        self.header_smart_values = header_smart_values

    def _smart_parse_TFIELDS( self ):
        h = self.header_smart_values
        try:
            n_fields=h['TFIELDS']
        except KeyError:
            print "Warning: Fits header field TFIELDS was not found"
            return
        
        # We expect now to find the fields TTYPE## and TFORM## in the header
        # ## - denotes a number from 1 to n_fields
        # examples are:
        # TTYPE1  = 'Time    '
        # TFORM1  = '1D      '
        
        # the letters denote the data type...
        # we need to translate from Fits data type letter code to numpy
        # data type:
        type_map = {}
        type_map['L'] = '?'
        type_map['B'] = '|u1'
        type_map['I'] = '>u2'
        type_map['J'] = '>u4'
        type_map['K'] = '>u8'
        type_map['A'] = '|S'
        type_map['E'] = '>f4'
        type_map['D'] = '>f8'
    
        # We do something secial here .. since we are dealing with FACT fits 
        # files mostly, we will treat those columns special, that have to do
        # with Pixels. Therefore we need to know how many pixel there are.
        try:
            n_pixel = h['NPIX']
        except KeyError:
            n_pixel = None
            
        dtype_list = []
        for i in range(1,n_fields+1):
            name = h['TTYPE%d'%i].strip()
            form = h['TFORM%d'%i].strip()
            # the form is always a number and then a capital letter
            # like e.g.: "1234J", "1E" , ...
            try:
                num = int(form[:-1])
            except ValueError:
                #print "Warning: TFORM is not of form '%d%C' but only '%C'. I assume simply 1."
                num = 1
            letter = form[-1]
            if letter == 'A':
                dtype_tuple = (name, "%s%d"%(type_map[letter],num))
            else:
                # Now we will check if this column might contain data
                # pixel-wise.
                if n_pixel is not None and num%n_pixel == 0:
                    num = (n_pixel, num/n_pixel)
                dtype_tuple = (name, type_map[letter], num)
            dtype_list.append( dtype_tuple )
            
        self.rawdata = np.recarray( 1, dtype=np.dtype(dtype_list) )
            
        

from datetime import datetime
class FadTempEvent (object):
    pass
class FadTempGetter( BasicEventGetter ):
    def next( self ):
        event = super(FadTempGetter, self).next()
        e = FadTempEvent()
        e.Time = datetime.utcfromtimestamp( event['Time']*86400 )
        e.QoS = event['QuS']
        e.temp = event['temp']
        return e

class RawEventGetter( BasicEventGetter ):
    
    def __init__( self, filename ):
        super(RawEventGetter, self).__init__(filename)
        
        int_keys = ['NROI', 'NAXIS1', 'NAXIS2', 'TSTARTI', 'TSTOPI', 'NBOARD']
        float_keys = ['TSTARTF', 'TSTOPF']
        self._parse_header( int_keys, float_keys )
        roi = self.header_values['NROI']
        
        # And now we define an numpy.recarray
        # which will be used to hold one single event
        # The format of this array, can be determined
        # from the seconds header, but I hardcode it here, because I'm lazy.
        self.rawdata = np.recarray(1, dtype = np.dtype([
            ('EventNum', '>u4'), 
            ('TriggerNum', '>u4'), 
            ('TriggerType', '>u2'), 
            ('NumBoards', '>u4'), 
            ('Errors', '|u1', 4), 
            ('SoftTrig', '>u4'), 
            ('UnixTimeUTC', '>u4', 2), 
            ('BoardTime', '>u4', 40), 
            ('StartCellData', '>u2', 1440), 
            ('StartCellTimeMarker', '>u2', 160), 
            ('Data', '>i2', (1440,roi) ) 
            ] ) )


class CalibrationConstantGetter( BasicEventGetter ):
    
    def __init__( self, filename ):
        super(CalibrationConstantGetter, self).__init__(filename)
        
        self._parse_header( int_keys=['NROI','NBOFFSET','NBGAIN','NBTRGOFF'], float_keys=[] )
        roi = self.header_values['NROI']
        
        self.rawdata = np.recarray(1, dtype = np.dtype([
            ('RunNumberBaseline', '>u4'), 
            ('RunNumberGain', '>u4'), 
            ('RunNumberTriggerOffset', '>u4'),
            ('BaselineMean', '>f4', (1440,1024)),
            ('BaselineRms', '>f4', (1440,1024)),
            ('GainMean', '>f4', (1440,1024)),
            ('GainRms', '>f4', (1440,1024)),
            ('TriggerOffsetMean', '>f4', (1440,roi)),
            ('TriggerOffsetRms', '>f4', (1440,roi)) ]) )
            
class EventGetter( object ):
    def __init__(self, data_file_name, calib_file_name ):
        
        self.data_getter = RawEventGetter( data_file_name )
        self.calib_file = CalibrationConstantGetter( calib_file_name )
        self.calib = self.calib_file.next()
        
        blm = self.calib['BaselineMean'][0]
        gm  = self.calib['GainMean'][0]
        self.tom = self.calib['TriggerOffsetMean'][0]
        self.blm = np.concatenate( (blm,blm), axis=1 )
        self.gm = np.concatenate( (gm,gm), axis=1 )

    def __iter__(self):
        """ iterator """
        return self

    def next(self):
        self.event = self.data_getter.next()
        return self.calibrate_drs_amplitude( )
        
    def calibrate_drs_amplitude(self):
        # shortcuts
        data = self.event['Data'][0]
        start_cells = self.event['StartCellData'][0]
        
        to_mV = 2000./4096.
        data = data.astype('float32')
        data *= to_mV
        roi = self.data_getter.header_values['NROI']
        
        for p in range(self.event['NumBoards'][0] * 36):
            pdata = data[p]
            sc = start_cells[p]
            pdata -= self.blm[p,sc:sc+roi]
            pdata -= self.tom[p,:roi]
            pdata /= self.gm[p,sc:sc+roi]
            data[p] = pdata
        
        data *= 1907.35
        
        return data

if __name__ == '__main__':
  import sys
  eg = BasicEventGetter(sys.argv[1])

