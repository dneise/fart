import readline
import rlcompleter
import re
readline.parse_and_bind('tab: complete')
readline.parse_and_bind('Control-Space: complete')
readline.set_completer_delims(re.compile(r'[\'"\\[]').sub('', readline.get_completer_delims())) 


import gzip
import numpy as np

class BasicEventGetter ( object ):
    def __init__( self, filename ):
        
        if ".gz" in filename:
            self._f = gzip.GzipFile(filename)
        else:
            self._f = open(filename)
        
        # our FITS files have two headers, lets read the first chunk.
        # headers consist of multiples of 2880 bytes
        h_chunk = self._f.read(2880)
        # we need to split up the header, into "lines" of 80 bytes
        # this makes 36 lines of 80 bytes in a chunk of 2880
        h1 = [ h_chunk[s*80:(s+1)*80] for s in range(36) ]
        # in order to find out, if this chunk, was a complete 
        # header, one of the lines must contain the letters "END" 
        # in the first 3 bytes
        end_found = False
        for line in h1:
            if line[0:3] == "END":
                end_found = True
                break
        if not end_found:
            raise IOError("first header was longer than 2880 bytes. Strange!")
        self._h1 = h1
        
        # Now we need to read the second header, this header
        # might change in size
        # so let's read the first chunk
        h_chunk = self._f.read(2880)
        h2 = [ h_chunk[s*80:(s+1)*80] for s in range(36) ]
        end_found = False
        for line in h2:
            if line[0:3] == "END":
                end_found = True
                break
        while not end_found:
            h_chunk = self._f.read(2880)
            h2 += [ h_chunk[s*80:(s+1)*80] for s in range(36) ]
            for line in h2:
                if line[0:3] == "END":
                    end_found = True
                    break
        self._h2 = h2

        # This member needs to be set by somebody
        self.rawdata = None
    
    def __iter__( self ):
        return self
        
    def next( self ):
        # read unzipped data
        s = self._f.read( self.rawdata.dtype.itemsize )
        try:
            event = np.fromstring(s, dtype=self.rawdata.dtype, count=1)
        except ValueError:
            raise StopIteration
        return event

        
class RawEventGetter( BasicEventGetter ):
    
    def __init__( self, filename ):
        super(RawEventGetter, self).__init__(filename)
        
        # now we need one single info from the seconds header.
        # This is the value NROI
        for line in self._h2:
            if "NROI" in line:
                line = line.split()
                if line[0] == "NROI":
                    self.roi = int(line[2])
            if "NAXIS2" in line:
                line = line.split()
                self.nevents = int(line[2])
            if "TSTARTI" in line:
                line = line.split()
                self.tstarti = int(line[2])
            if "TSTARTF" in line:
                line = line.split()
                self.tstartf = float(line[2])
            if "TSTOPI" in line:
                line = line.split()
                self.tstopi = int(line[2])
            if "TSTOPF" in line:
                line = line.split()
                self.tstopf = float(line[2])
        
        # And now we define an numpy.recarray
        # which will be used to hold one single event
        # The format of this array, can be determined
        # from the seconds header, but I hardcode it here, because I'm lazy.
        self.rawdata = np.recarray(1, dtype = np.dtype([
            ('EventNum', '>u4'), 
            ('TriggerNum', '>u4'), 
            ('TriggerType', '>u2'), 
            ('NumBoards', '>u4'), 
            ('Errors', '|u1', 4), 
            ('SoftTrig', '>u4'), 
            ('UnixTimeUTC', '>u4', 2), 
            ('BoardTime', '>u4', 40), 
            ('StartCellData', '>u2', 1440), 
            ('StartCellTimeMarker', '>u2', 160), 
            ('Data', '>i2', (1440,self.roi) ) 
            ] ) )


class CalibrationConstantGetter( BasicEventGetter ):
    
    def __init__( self, filename ):
        super(CalibrationConstantGetter, self).__init__(filename)
        
        # now we need one single info from the seconds header.
        # This is the value NROI
        for line in self._h2:
            if "NROI" in line:
                line = line.split()
                self.roi = int(line[2])
                break
        
        self.rawdata = np.recarray(1, dtype = np.dtype([
            ('RunNumberBaseline', '>u4'), 
            ('RunNumberGain', '>u4'), 
            ('RunNumberTriggerOffset', '>u4'),
            ('BaselineMean', '>f4', (1440,1024)),
            ('BaselineRms', '>f4', (1440,1024)),
            ('GainMean', '>f4', (1440,1024)),
            ('GainRms', '>f4', (1440,1024)),
            ('TriggerOffsetMean', '>f4', (1440,self.roi)),
            ('TriggerOffsetRms', '>f4', (1440,self.roi)) ]) )
            
class EventGetter( object ):
    def __init__(self, data_file_name, calib_file_name ):
        self.data_getter = RawEventGetter( data_file_name )
        calib_file = CalibrationConstantGetter( calib_file_name )
        self.calib_file =calib_file
        calib = calib_file.next()
        self.calib = calib
        self.roi = self.data_getter.roi
        self.nevents = self.data_getter.nevents
        self.tstarti = self.data_getter.tstarti
        self.tstartf = self.data_getter.tstartf
        self.tstopi = self.data_getter.tstopi
        self.tstopf = self.data_getter.tstopf
        print self.tstarti
        print self.tstartf
        print self.tstopi
        print self.tstopf

    def __iter__(self):
        """ iterator """
        return self

    def next(self):
        self.event = self.data_getter.next()
        return self.calibrate_drs_amplitude( )
        
    def calibrate_drs_amplitude(self):
        # shortcuts
        blm = self.calib['BaselineMean'][0]
        gm  = self.calib['GainMean'][0]
        tom = self.calib['TriggerOffsetMean'][0]

        blm = np.concatenate( (blm,blm), axis=1 )
        gm = np.concatenate( (gm,gm), axis=1 )

        to_mV = 2000./4096.
        data = self.event['Data'][0]
        data = data.astype('float32')
        start_cells = self.event['StartCellData'][0]
        data *= to_mV
        
        for p in range(self.event['NumBoards'][0] * 36):
            pdata = data[p]
            sc = start_cells[p]
            pdata -= blm[p,sc:sc+self.roi]
            pdata -= tom[p,:self.roi]
            pdata /= gm[p,sc:sc+self.roi]
            data[p] = pdata
        
        data *= 1907.35
        
        return data
