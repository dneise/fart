#!/usr/bin/python -tt
#
# Dominik Neise, Werner Lustermann
# TU Dortmund, ETH Zurich
#
import numpy as np
from scipy import signal
import fir_filter

def test_SlidingAverage():
    """ test the sliding average function
        A constant input should result in a constant and equal output
    """
    safilter = fir_filter.SlidingAverage(8)
    signal = np.ones(100) * 3.0
    filtered = safilter(signal)
    assert (filtered == signal).all()
    

if __name__ == '__main__':
    """ conduct the tests """
    
    test_SlidingAverage()
