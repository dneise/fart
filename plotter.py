#!/usr/bin/python -tt
#
# Werner Lustermann
# ETH Zurich
#
# plotter.py

import numpy as np
import matplotlib.pyplot as plt

class Plotter(object):
    """ simple x-y plot """
    def __init__(self, name, x, style = 'b', xlabel='x', ylabel='y'):
        """ initialize the object """
        
        self.name  = name
        self.fig   = plt.figure()
        self.line, = plt.plot(x, style)
        
        plt.title(name)
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        plt.grid(True)
           
    def __call__(self, ydata):
        """ set ydata of plot """
        plt.figure(self.fig.number)
        plt.ylim( np.min(ydata), np.max(ydata) )
        self.line.set_ydata(ydata)
        plt.draw()
            
def _test():
    """ test of maintaining two independant plotter instances """
    
    plt.ion()
    
    x = np.linspace(0., 10.)
    plot1 = Plotter('plot1', x, 'r')
    print 'plot1.fig.number: ', plot1.fig.number
    plot2 = Plotter('plot2', x, 'g.')
    print 'plot2.fig.number: ', plot2.fig.number
    
    plot1(np.sin(x) * 7.)
    plot2(x*x)
    
    raw_input('next')
    
    plot1(np.cos(x) * 3.)
    plot2(x)
    
    raw_input('next')
    
    
    
if __name__ == '__main__':
    """ test the class """
    
    #_test_UniqueID(0)
    
    _test()
