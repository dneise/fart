#!/usr/bin/python -tt

import sys
import os.path as path
import numpy as np
from ROOT import *

import fart.io_utils.FitsReader_new as FitsReader
import fart.drs_spikes as drs_spikes


data_filepath = sys.argv[1]
calib_filepath = sys.argv[2]
out_filepath = sys.argv[3]
data_filename = path.split( data_filepath )[1]
print "data filename:", data_filename

f = FitsReader.EventGetter( data_filepath, calib_filepath )
tfile = TFile(out_filepath, "RECREATE")

number_of_events = f.data_getter.header_values['NAXIS2']


number_of_pixel = f.data_getter.header_values['NBOARD'] * 36
roi = f.data_getter.header_values['NROI']
print "number of events - from header:", number_of_events
print "number_of_pixel:", number_of_pixel
print "roi:", roi


ttree = TTree('tree', 'a tree of data from FACt fitsfiles')

# we need one single numpy array, which stays in memory, so the TTree 
# can have a pointer on it. So we make it here
fitsdata = np.empty( roi, dtype=np.float32 )
dummytime = np.arange( roi, dtype=np.float32 )
pixelid = np.empty( 1, dtype=np.int32)
eventid = np.empty( 1, dtype=np.int32)

ttree.Branch('data', fitsdata , 'data['+str(roi)+']/F')
ttree.Branch('time', dummytime , 'time['+str(roi)+']/F')
ttree.Branch('pixelid', pixelid , 'pixelid/I')
ttree.Branch('eventid', eventid , 'eventid/I')



# We are especially interested in the duration of a run, 
# and since the start and stoptime of a run, is stored in a funny way in the 
# FITS files, we have to treat them a bit, before we get the actual numbers
# shortcuts:
header = f.data_getter.header_values
# time_stamps are in seconds since 01.01.1970  - unix epoch
stop_time_stamp = (header['TSTOPI'] + header['TSTOPF']) * 24 * 60 * 60 
start_time_stamp = (header['TSTARTI'] + header['TSTARTF']) * 24 * 60 * 60 
duration = stop_time_stamp - start_time_stamp
print "duration",duration
for name, value in f.data_getter.header_values.iteritems():
  print name+":", value

despiker = drs_spikes.DRSSpikes()

for index,event in enumerate(f):
    print "event:", index
    data = despiker(event)
    for pixel_id, pixeldata in enumerate(data[:number_of_pixel]):
        print "   pixel_id", pixel_id
        fitsdata[:] = pixeldata
        pixelid[0] = pixel_id
        eventid[0] = index
        ttree.Fill()

tfile.Write()
ttree.Write()

tfile.Close()
