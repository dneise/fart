Welcome to FaRT - some FACT rawdata access tools.

This tiny pack of clases is inteded to give you quick (and sometimes dirty)
access to FACT rawdata using Python.

This is not ready to use software, but just a little bundle of classes that 
might help you accessing the data. So you need to write the scripts yourself.

------------------------------------------------------------------------------

There is not much to install. Just check the stuff out onto you disk.
It might make sense, to set your PYTHONPATH to your checkout folder.

example:

    dneise@neiseLenovo:~/FART/fart$ ls
    README.md __init__.py   io_utils  example.py  [...and more...]
    dneise@neiseLenovo:~/FART/fart$ pwd
    /home/dneise/FART/fart
    dneise@neiseLenovo:~/FART/fart$ export PYTHONPATH=$PYTHONPATH:/home/dneise/FART

With this setting you can write you scripts where ever you like,
since Python will find the fart folder.

------------------------------------------------------------------------------

As a test you can copy the example.py somewhere else, e.g. put it into the folder 
with your .fits files. And then go there and type:

    ./example.py 20130310_045.fits 20130310_022.drs.fits
    
    or 
    
    ./example.py data.fits.gz calib.drs.fits.gz
    
If you have matplotlib correctly installed you will get a plot with the Data
of all pixels plottet.
Hit <Enter> for the next event

Hit Ctrl-d to stop the loop over all events and fall back into the Python 
Interpreter.

In the Interpreter you can still look at the data of the last event in the loop,
e.g. 

    >>> data.shape
    (1440, 1024)
    >>> data[0]
    array([ 469.15512085,  472.14990234,  477.71505737, ...,  465.33486938,
        464.41622925,  464.74560547], dtype=float32)


If you want the next event, type:
    
    >>> event = f.next()

But the data is not automatically cleaned form DRS spikes... 
you can do that by

    >>> event = despiker(event)


------------------------------------------------------------------------------

But now have a look into the example and start writing your own application.
Have Fun!
