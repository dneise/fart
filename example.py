#!/usr/bin/python -tt

import sys
import fart.io_utils.FitsReader as FitsReader
import fart.drs_spikes as drs_spikes
import h5py

import matplotlib.pyplot as plt
plt.ion()
    
f = FitsReader.EventGetter( sys.argv[1], sys.argv[2] )
print "number of events", f.data_getter.nevents

outfile = h5py.File(sys.argv[3], "w")
dset = outfile.create_dataset("MyDataset", (36, f.roi, f.nevents), 'f4')

dset.attrs["NROI"]     = f.data_getter.roi
dset.attrs["NEVENTS"]  = f.data_getter.nevents
dset.attrs["TSTARTI"]  = f.data_getter.tstarti
dset.attrs["TSTARTF"]  = f.data_getter.tstartf
dset.attrs["TSTOPI"]   = f.data_getter.tstopi
dset.attrs["TSTOPF"]   = f.data_getter.tstopf
dset.attrs["DELTAT"]   = ( (f.data_getter.tstopi + f.data_getter.tstopf) - (f.data_getter.tstarti + f.data_getter.tstartf) ) * 24 * 3600

for name, value in dset.attrs.iteritems():
  print name+":", value

despiker = drs_spikes.DRSSpikes()

for index,event in enumerate(f):
    print "event:", index
    data = despiker(event)
    dset[..., index] = data[0:36]

outfile.close()
